
import { ApiRequest } from '../index'

import moxios from 'moxios'


describe("ApiRequest", () => {
  beforeEach(function () {
    moxios.install()
  })

  afterEach(function () {
    moxios.uninstall()
  })

  it("status 200 res code OK", async () => {
    moxios.stubRequest(/.*/, { status: 200, response: { code: "OK" } })
    var res = await ApiRequest.get(`/a`)
    expect(res).toHaveProperty("code")
  })

  it("status 200 res code not OK", async () => {
    moxios.stubRequest(/.*/, { status: 200, response: { code: "XXXX" } })
    try {
      var res = await ApiRequest.get(`/a`)
    } catch (err) {
      expect(err).toHaveProperty("code", "XXXX")
    }
  })

  it("status 200 res empty", async () => {
    moxios.stubRequest(/.*/, { status: 200 })
    try {
      var res = await ApiRequest.get(`/a`)
    } catch (err) {
      expect(err).toHaveProperty("code", "C140")
    }
  })

  it("status 200 res code empty", async () => {
    moxios.stubRequest(/.*/, { status: 200, response: {} })
    try {
      var res = await ApiRequest.get(`/a`)
    } catch (err) {
      expect(err).toHaveProperty("code", "C130")
    }
  })

  it("status 200 res code OK with data and message", async () => {
    moxios.stubRequest(/.*/, { status: 200, response: { code: "OK", data: "datainres", message: "messagefromserver" } })

    var res = await ApiRequest.get(`/a`)
    expect(res).toHaveProperty("code", "OK")
    expect(res).toHaveProperty("data", "datainres")
    expect(res).toHaveProperty("message", "messagefromserver")
  })

  it("status 200 res code not OK with data and message", async () => {
    moxios.stubRequest(/.*/, { status: 200, response: { code: "XXXX", data: "datainres", message: "messagefromserver" } })
    try {
      var res = await ApiRequest.get(`/a`)
    } catch (err) {
      expect(err).toHaveProperty("code", "XXXX")
      expect(err).toHaveProperty("data", "datainres")
      expect(err).toHaveProperty("message", "messagefromserver")
    }
  })

  it("status 401", async () => {
    moxios.stubRequest(/.*/, { status: 401 })
    try {
      var res = await ApiRequest.get(`/a`)
    } catch (err) {
      expect(err).toHaveProperty("code", 401)
    }
  })
  it("status 403", async () => {
    moxios.stubRequest(/.*/, { status: 403 })
    try {
      var res = await ApiRequest.get(`/a`)
    } catch (err) {
      expect(err).toHaveProperty("code", 403)
    }
  })
  it("status 404", async () => {
    moxios.stubRequest(/.*/, { status: 404 })
    try {
      var res = await ApiRequest.get(`/a`)
    } catch (err) {
      expect(err).toHaveProperty("code", 404)
    }
  })
  it("status 500", async () => {
    moxios.stubRequest(/.*/, { status: 500 })
    try {
      var res = await ApiRequest.get(`/a`)
    } catch (err) {
      expect(err).toHaveProperty("code", 500)
    }
  })

})
