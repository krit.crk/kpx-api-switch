import { CodeSwitch } from '../index'

describe("ApiRequest", () => {
    it("code empty call C130", () => {
        const mockMustCallback = jest.fn();
        const mockNotCallback1 = jest.fn();
        const mockNotCallback2 = jest.fn();
        CodeSwitch({}, {
            "C130": mockMustCallback,
            "ERROR": mockNotCallback1,
            "XXXX": mockNotCallback2,
        })
        expect(mockMustCallback.mock.calls.length).toBe(1);
        expect(mockNotCallback1.mock.calls.length).toBe(0);
        expect(mockNotCallback2.mock.calls.length).toBe(0);
    })

    it("code empty call ERROR", () => {
        const mockMustCallback = jest.fn();
        const mockNotCallback1 = jest.fn();
        const mockNotCallback2 = jest.fn();
        CodeSwitch({}, {
            "ERROR": mockMustCallback,
            "XXXX": mockNotCallback2,
        })
        expect(mockMustCallback.mock.calls.length).toBe(1);
        expect(mockNotCallback1.mock.calls.length).toBe(0);
        expect(mockNotCallback2.mock.calls.length).toBe(0);
    })

    it("code empty call not fn C130", () => {
        const mockMustCallback = jest.fn();
        console.error = jest.fn()
        CodeSwitch({}, {
            "ERROR": mockMustCallback,
            "C130": "not fn",
        })
        expect(mockMustCallback.mock.calls.length).toBe(1);
        expect(console.error.mock.calls.length).toBe(0);
    })

    it("code empty call not fn C130 not fr err ", () => {
        const mockMustCallback = jest.fn();
        console.error = jest.fn()
        CodeSwitch({}, {
            "ERROR": "not fn",
            "C130": "not fn",
        })
        expect(console.error.mock.calls.length).toBe(1);
    })

    it("code XXXX call not fn XXXX", () => {
        const mockMustCallback = jest.fn();
        console.error = jest.fn()
        CodeSwitch({ code: "XXXX" }, {
            "ERROR": mockMustCallback,
            "XXXX": "not fn",
        })
        expect(mockMustCallback.mock.calls.length).toBe(1);
        expect(console.error.mock.calls.length).toBe(0);
    })

    it("code XXXX call no fn XXXX", () => {
        const mockMustCallback = jest.fn();
        console.error = jest.fn()
        CodeSwitch({ code: "XXXX" }, {
            "ERROR": mockMustCallback,
            "XXXX": "not fn",
        })
        expect(mockMustCallback.mock.calls.length).toBe(1);
        expect(console.error.mock.calls.length).toBe(0);
    })

    it("code XXXX call not fn XXXX not fr err ", () => {
        console.error = jest.fn()
        CodeSwitch({ code: "XXXX" }, {
            "ERROR": "not fn",
            "XXXX": "not fn",
        })
        expect(console.error.mock.calls.length).toBe(1);
    })


    it("code XXXX call no fn XXXX no fr err ", () => {
        console.error = jest.fn()
        CodeSwitch({ code: "XXXX" }, {})
        expect(console.error.mock.calls.length).toBe(1);
    })


    it("code XXXX call XXXX", () => {
        const mockMustCallback = jest.fn();
        const mockNotCallback1 = jest.fn();
        const mockNotCallback2 = jest.fn();
        CodeSwitch({ code: "XXXX" }, {
            "C130": mockNotCallback2,
            "ERROR": mockNotCallback1,
            "XXXX": mockMustCallback,
        })
        expect(mockMustCallback.mock.calls.length).toBe(1);
        expect(mockNotCallback1.mock.calls.length).toBe(0);
        expect(mockNotCallback2.mock.calls.length).toBe(0);
    })


    it("code XXXX call ERROR", () => {
        const mockMustCallback = jest.fn();
        const mockNotCallback1 = jest.fn();
        const mockNotCallback2 = jest.fn();
        CodeSwitch({ code: "XXXX" }, {
            "ERROR": mockMustCallback,
            "error": mockNotCallback2,
        })
        expect(mockMustCallback.mock.calls.length).toBe(1);
        expect(mockNotCallback1.mock.calls.length).toBe(0);
        expect(mockNotCallback2.mock.calls.length).toBe(0);
    })
})