
import { ApiRequest, CodeSwitch } from '../index'

import moxios from 'moxios'


describe("Full Test", () => {
    beforeEach(function () {
        moxios.install()
    })

    afterEach(function () {
        moxios.uninstall()
    })

    it("status 401", async () => {
        moxios.stubRequest(/.*/, { status: 401, response: {} })
        const mockMustCallback = jest.fn();
        const mockNotCallback1 = jest.fn();
        const mockNotCallback2 = jest.fn();

        const sw = {
            "401": mockMustCallback,
            "ERROR": mockNotCallback2
        }
        try {
            var res = await ApiRequest.get(`/a`)
            mockNotCallback1()
        } catch (err) {
            CodeSwitch(err, sw)
        }

        expect(mockMustCallback.mock.calls.length).toBe(1);
        expect(mockNotCallback1.mock.calls.length).toBe(0);
        expect(mockNotCallback2.mock.calls.length).toBe(0);

    })

    it("empty", async () => {
        moxios.stubRequest(/.*/, { status: 200, response: {} })
        const mockMustCallback = jest.fn();
        const mockNotCallback1 = jest.fn();

        const sw = { "ERROR": mockMustCallback }
        try {
            var res = await ApiRequest.get(`/a`)
            mockNotCallback1()
        } catch (err) {
            CodeSwitch(err, sw)
        }

        expect(mockMustCallback.mock.calls.length).toBe(1);
        expect(mockNotCallback1.mock.calls.length).toBe(0);

    })

    it("code XXXX", async () => {
        moxios.stubRequest(/.*/, { status: 200, response: { code: "XXXX", data: "datainres", message: "messagefromserver" } })
        const mockMustCallback = jest.fn();
        const mockNotCallback1 = jest.fn();

        const sw = { "XXXX": mockMustCallback }
        try {
            var res = await ApiRequest.get(`/a`)
            mockNotCallback1()
        } catch (err) {
            CodeSwitch(err, sw)
            expect(err).toHaveProperty("code", "XXXX")
            expect(err).toHaveProperty("data", "datainres")
            expect(err).toHaveProperty("message", "messagefromserver")
        }

        expect(mockMustCallback.mock.calls.length).toBe(1);
        expect(mockNotCallback1.mock.calls.length).toBe(0);

    })
    it("code OK", async () => {
        moxios.stubRequest(/.*/, { status: 200, response: { code: "OK", data: "datainres", message: "messagefromserver" } })
        const mockMustCallback = jest.fn();
        const mockNotCallback1 = jest.fn();

        const sw = { "ERROR": mockNotCallback1 }
        try {
            var res = await ApiRequest.get(`/a`)
            mockMustCallback()
            expect(res).toHaveProperty("code", "OK")
            expect(res).toHaveProperty("data", "datainres")
            expect(res).toHaveProperty("message", "messagefromserver")
        } catch (err) {
            CodeSwitch(err, sw)
        }

        expect(mockMustCallback.mock.calls.length).toBe(1);
        expect(mockNotCallback1.mock.calls.length).toBe(0);

    })
})