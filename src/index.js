import Axios from 'axios'
import _ from 'lodash'


export class ApiRequest {
    static async get(url, config) {
        return this._custom("get", url, null, config)
    }
    static async post(url, data, config) {
        return this._custom("post", url, data, config)
    }
    static async put(url, data, config) {
        return this._custom("put", url, data, config)
    }
    static async patch(url, data, config) {
        return this._custom("patch", url, data, config)
    }
    static async delete(url, config) {
        return this._custom("delete", url, null, config)
    }
    static async _custom(ac, url, data, config) {
        let res;
        try {
            switch (ac) {
                case "get": res = await Axios.get(url, config); break;
                case "delete": res = await Axios.delete(url, config); break;
                case "post": res = await Axios.post(url, data, config); break;
                case "put": res = await Axios.put(url, data, config); break;
                case "patch": res = await Axios.patch(url, data, config); break;
                default: throw {
                    response: { status: "C110" },
                    message: "invalid request action"
                }
            }
        } catch (err) {
           // console.log(err);
            throw {
                code: err.response && err.response.status || "ERROR",
                message: err.message,
                data: err.response && err.response.data,
            }
        }

        if (res.status < 200 && res.status > 299)
            throw {
                code: "C120",
                message: "invalid response status ",
                data: res.data
            };

        if (!res.data)
            throw {
                code: "C140",
                message: "invalid response",
                data: res.data
            };

        if (!res.data.code)
            throw {
                code: "C130",
                message: "invalid response code",
                data: res.data
            };

        if (res.data.code != "OK")
            throw res.data;

        return res.data;
    }
}

export function CodeSwitch(err, sw) {
    const error = _.isFunction(sw["ERROR"]) ? sw["ERROR"] : (err) => console.error(err)
    if (!err.code) {
        if (_.isFunction(sw["C130"])) return sw["C130"](err)
        else return error(err)
    }

    if (_.isFunction(sw[err.code])) return sw[err.code](err)
    else return error(err)
}